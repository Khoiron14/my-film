package com.khoiron.myfilm;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.khoiron.myfilm.data.model.User;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {
    @BindView(R.id.progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.edt_email) EditText mInputEmail;
    @BindView(R.id.edt_password) EditText mInputPassword;
    @BindView(R.id.edt_username) EditText mInputUsername;
    @BindView(R.id.btn_register) Button mButtonRegister;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirebaseDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseFirestore.getInstance();

        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInputEmail.getText().toString().equals("") || mInputPassword.getText().toString().equals("") || mInputUsername.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Semua inputan harus diisi!", Toast.LENGTH_LONG).show();
                } else {
                    mButtonRegister.setEnabled(false);
                    mProgressBar.setVisibility(View.VISIBLE);

                    createUser();
                }
            }
        });

        getSupportActionBar().setTitle("Register");
    }

    private void createUser() {
        mFirebaseAuth.createUserWithEmailAndPassword(
                mInputEmail.getText().toString(),
                mInputPassword.getText().toString())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        FirebaseUser user = authResult.getUser();

                        User newUser = new User();
                        newUser.setUid(user.getUid());
                        newUser.setEmail(user.getEmail());
                        newUser.setUsername(mInputUsername.getText().toString());

                        mFirebaseDatabase.collection("users/").document(user.getUid())
                                .set(newUser)
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        mProgressBar.setVisibility(View.GONE);
                                        finish();
                                        Toast.makeText(getApplicationContext(), "Berhasil Registrasi!", Toast.LENGTH_SHORT).show();
                                    }
                                })
                                .addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        mProgressBar.setVisibility(View.GONE);
                                        finish();
                                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mButtonRegister.setEnabled(true);
                        mProgressBar.setVisibility(View.GONE);
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
