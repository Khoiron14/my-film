package com.khoiron.myfilm.data;

import android.app.Application;
import android.arch.persistence.room.Room;

public class MyFilm extends Application {
    public static AppRoomDatabase database;

    @Override
    public void onCreate() {
        super.onCreate();
        database = Room.databaseBuilder(getApplicationContext(), AppRoomDatabase.class, "favoritemovie.db")
                .allowMainThreadQueries().build();
    }
}
