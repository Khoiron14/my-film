package com.khoiron.myfilm.data.model;

import java.util.Date;

public class Comment {
    private String uid, tmdbid, username, imguser, message, time;
    private Date timestamp;

    public Comment() {
    }

    public Comment(String uid, String tmdbid, String username, String imguser, String message, String time, Date timestamp) {
        this.uid = uid;
        this.tmdbid = tmdbid;
        this.username = username;
        this.imguser = imguser;
        this.message = message;
        this.time = time;
        this.timestamp = timestamp;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getTmdbid() {
        return tmdbid;
    }

    public void setTmdbid(String tmdbid) {
        this.tmdbid = tmdbid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImguser() {
        return imguser;
    }

    public void setImguser(String imguser) {
        this.imguser = imguser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}
