package com.khoiron.myfilm.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.khoiron.myfilm.data.model.MovieFavorite;

@Database(entities = {MovieFavorite.class}, version = 1)
public abstract class AppRoomDatabase extends RoomDatabase {
    public abstract MovieFavoriteDao mMovieFavoriteDao();
}
