package com.khoiron.myfilm.data;

public class Api {
    private static String KEY = "97989c64195b074a793a6661268305b4";
    private static  String BASE_URL = "http://api.themoviedb.org/3/";
    public static String POSTER_PATH = "https://image.tmdb.org/t/p/w600_and_h900_bestv2";

    public static String MOVIE_POPULAR(String page) {
        return BASE_URL + "movie/popular?api_key=" + KEY + "&language=en-US&page=" + page;
    }

    public static String MOVIE_UPCOMING(String page) {
        return BASE_URL + "movie/upcoming?api_key=" + KEY + "&language=en-US&page=" + page;
    }
    public static String MOVIE_TOPRATED(String page) {
        return BASE_URL + "movie/top_rated?api_key=" + KEY + "&language=en-US&page="+ page;
    }

    public static String MOVIE(String id) {
        return BASE_URL + "movie/" + id + "?api_key=" + KEY + "&append_to_response=videos";
    }
}
