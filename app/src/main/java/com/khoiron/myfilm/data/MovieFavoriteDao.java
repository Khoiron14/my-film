package com.khoiron.myfilm.data;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.khoiron.myfilm.data.model.MovieFavorite;

import java.util.List;

@Dao
public interface MovieFavoriteDao {
    @Query("select * from MovieFavorite")
    List<MovieFavorite> getAll();

    @Query("select * from MovieFavorite where tmdbId = :tmdbId")
    MovieFavorite getMovie(String tmdbId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MovieFavorite newMovieFavorite);

    @Update
    void update(MovieFavorite movieFavorite);

    @Delete
    void delete(MovieFavorite movieFavorite);
}
