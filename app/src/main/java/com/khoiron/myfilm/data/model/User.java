package com.khoiron.myfilm.data.model;

public class User {
    private String uid, email, username, imageurl;

    public User() {
    }

    public User(String uid, String email, String username, String imageurl) {
        this.uid = uid;
        this.email = email;
        this.username = username;
        this.imageurl = imageurl;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImageurl() {
        return imageurl;
    }

    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }
}
