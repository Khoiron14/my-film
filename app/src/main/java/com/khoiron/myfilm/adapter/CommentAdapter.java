package com.khoiron.myfilm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khoiron.myfilm.R;
import com.khoiron.myfilm.data.model.Comment;
import com.khoiron.myfilm.holder.CommentHolder;
import com.khoiron.myfilm.utils.DownloadImage;

import java.util.ArrayList;

public class CommentAdapter extends RecyclerView.Adapter<CommentHolder> {
    private ArrayList<Comment> mComments;

    public CommentAdapter(ArrayList<Comment> comments) {
        this.mComments = comments;
    }

    @NonNull
    @Override
    public CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_comment, parent,false);

        return new CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
        final Comment comment = mComments.get(position);

        DownloadImage.picasso(comment.getImguser(), holder.mImageUser);
        holder.mUsername.setText(comment.getUsername());
        holder.mTextTime.setText(comment.getTime().toString());
        holder.mTextComment.setText(comment.getMessage());
    }

    @Override
    public int getItemCount() {
        return mComments.size();
    }
}
