package com.khoiron.myfilm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khoiron.myfilm.R;
import com.khoiron.myfilm.data.Api;
import com.khoiron.myfilm.data.model.Movie;
import com.khoiron.myfilm.data.model.MovieFavorite;
import com.khoiron.myfilm.holder.MovieHolder;
import com.khoiron.myfilm.utils.DownloadImage;

import java.util.ArrayList;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<MovieHolder> {
    private ArrayList<Movie> mMovies;
    private ListOnClick mListener;

    public MovieAdapter(ArrayList<Movie> movies) {
        this.mMovies = movies;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movie, parent,false);

        mListener = (ListOnClick) parent.getContext();
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final Movie movie = mMovies.get(position);

        holder.textTitle.setText(movie.getTitle());
        DownloadImage.picasso(Api.POSTER_PATH + movie.getPoster(), holder.imagePoster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.mClick(movie);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMovies.size();
    }

    public interface ListOnClick {
        void mClick(Movie movie);
    }

}
