package com.khoiron.myfilm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khoiron.myfilm.R;
import com.khoiron.myfilm.data.Api;
import com.khoiron.myfilm.data.model.MovieFavorite;
import com.khoiron.myfilm.holder.MovieHolder;
import com.khoiron.myfilm.utils.DownloadImage;

import java.util.List;

public class FavoriteMovieAdapter extends RecyclerView.Adapter<MovieHolder> {
    private List<MovieFavorite> mMovieFavorites;
    private ListOnClick mListener;

    public FavoriteMovieAdapter(List<MovieFavorite> movieFavorites) {
        this.mMovieFavorites = movieFavorites;
    }

    @NonNull
    @Override
    public MovieHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_movie, parent,false);

        mListener = (ListOnClick) parent.getContext();
        return new MovieHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieHolder holder, int position) {
        final MovieFavorite movieFavorite = mMovieFavorites.get(position);

        holder.textTitle.setText(movieFavorite.getTitle());
        DownloadImage.picasso(Api.POSTER_PATH + movieFavorite.getPosterPath(), holder.imagePoster);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.mClick(movieFavorite);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mMovieFavorites.size();
    }

    public interface ListOnClick {
        void mClick(MovieFavorite movieFavorite);
    }
}
