package com.khoiron.myfilm.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.khoiron.myfilm.MovieDetailActivity;
import com.khoiron.myfilm.R;
import com.khoiron.myfilm.data.model.Video;
import com.khoiron.myfilm.holder.VideoHolder;

import java.util.ArrayList;
import java.util.List;

public class VideoAdapter extends RecyclerView.Adapter<VideoHolder> {
    private ArrayList<Video> mVideos;
    private ListOnClick mListener;

    public VideoAdapter(ArrayList<Video> videos) {
        this.mVideos = videos;
    }

    @NonNull
    @Override
    public VideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_video, parent, false);

        mListener = (ListOnClick) parent.getContext();
        return new VideoHolder(view) ;
    }

    @Override
    public void onBindViewHolder(@NonNull VideoHolder holder, int position) {
        final Video video = mVideos.get(position);

        holder.textName.setText(video.getName());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.mClick(video);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mVideos.size();
    }

    public interface ListOnClick {
        void mClick(Video video);
    }
}
