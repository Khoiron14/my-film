package com.khoiron.myfilm.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.khoiron.myfilm.R;

public class MovieHolder extends RecyclerView.ViewHolder {

    public TextView textTitle;
    public ImageView imagePoster;

    public MovieHolder(View itemView) {
        super(itemView);

        textTitle = itemView.findViewById(R.id.txt_title);
        imagePoster = itemView.findViewById(R.id.img_poster);
    }
}
