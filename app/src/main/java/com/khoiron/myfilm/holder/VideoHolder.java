package com.khoiron.myfilm.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.khoiron.myfilm.R;

public class VideoHolder extends RecyclerView.ViewHolder {
    public TextView textName;

    public VideoHolder(View itemView) {
        super(itemView);

        textName = itemView.findViewById(R.id.txt_title);
    }
}
