package com.khoiron.myfilm.holder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.khoiron.myfilm.R;

public class CommentHolder extends RecyclerView.ViewHolder {
    public ImageView mImageUser;
    public TextView mUsername;
    public TextView mTextTime;
    public TextView mTextComment;

    public CommentHolder(View itemView) {
        super(itemView);

        mImageUser = itemView.findViewById(R.id.img_user);
        mUsername = itemView.findViewById(R.id.txt_username);
        mTextTime = itemView.findViewById(R.id.txt_time);
        mTextComment = itemView.findViewById(R.id.txt_comment);
    }
}
