package com.khoiron.myfilm;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.khoiron.myfilm.adapter.FavoriteMovieAdapter;
import com.khoiron.myfilm.adapter.MovieAdapter;
import com.khoiron.myfilm.adapter.ViewPagerAdapter;
import com.khoiron.myfilm.data.model.Movie;
import com.khoiron.myfilm.data.model.MovieFavorite;
import com.khoiron.myfilm.fragment.FavoriteFragment;
import com.khoiron.myfilm.fragment.PopularFragment;
import com.khoiron.myfilm.fragment.TopRatedFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MovieAdapter.ListOnClick, FavoriteMovieAdapter.ListOnClick {
    @BindView(R.id.tab_layout) TabLayout mTabLayout;
    @BindView(R.id.view_pager) ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        addTabs(mViewPager);
        mTabLayout.setupWithViewPager(mViewPager);
        mTabLayout.setSelectedTabIndicatorColor(Color.parseColor("#3F51B5"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_user:
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                if (user != null) {
                    startActivity(new Intent(getApplicationContext(), ProfileActivity.class));
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void addTabs(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new PopularFragment(), "Popular");
        adapter.addFrag(new TopRatedFragment(), "Top Rated");
        adapter.addFrag(new FavoriteFragment(), "Favorite");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void mClick(Movie movie) {
        Intent n = new Intent(MainActivity.this, MovieDetailActivity.class);
        n.putExtra("movie_id", movie.getId());
        n.putExtra("title", movie.getTitle());
        n.putExtra("posterPath", movie.getPoster());
        startActivity(n);
    }

    @Override
    public void mClick(MovieFavorite movieFavorite) {
        Intent n = new Intent(MainActivity.this, MovieDetailActivity.class);
        n.putExtra("movie_id", movieFavorite.getTmdbId());
        n.putExtra("title", movieFavorite.getTitle());
        n.putExtra("posterPath", movieFavorite.getPosterPath());
        startActivity(n);
    }
}
