package com.khoiron.myfilm;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.khoiron.myfilm.adapter.VideoAdapter;
import com.khoiron.myfilm.data.Api;
import com.khoiron.myfilm.data.MyFilm;
import com.khoiron.myfilm.data.model.MovieFavorite;
import com.khoiron.myfilm.data.model.Video;
import com.khoiron.myfilm.utils.DownloadImage;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity implements VideoAdapter.ListOnClick {
    @BindView(R.id.img_cover) ImageView coverImage;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.img_poster) ImageView posterImage;
    @BindView(R.id.txt_date) TextView dateText;
    @BindView(R.id.txt_duration) TextView durationText;
    @BindView(R.id.txt_rating) TextView ratingText;
    @BindView(R.id.txt_sinopsis) TextView sinopsisText;
    @BindView(R.id.list_video) RecyclerView videoList;
    @BindView(R.id.fab_favorite) FloatingActionButton favBtn;

    ArrayList<Video> videos = new ArrayList<>();
    String movie_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        final Bundle bundle = getIntent().getExtras();
        movie_id = bundle.getString("movie_id");
        toolbar.setTitle(bundle.getString("title"));

        if (MyFilm.database.mMovieFavoriteDao().getMovie(movie_id) != null) {
            favBtn.setImageResource(R.drawable.ic_favorite);
        }

        loadMovie();

        // add or remove favorite movie
        favBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (MyFilm.database.mMovieFavoriteDao().getMovie(movie_id) != null) {
                    Toast.makeText(getApplicationContext(), "Favorite Deleted!", Toast.LENGTH_SHORT).show();

                    MovieFavorite favorite = MyFilm.database.mMovieFavoriteDao().getMovie(movie_id);

                    MyFilm.database.mMovieFavoriteDao().delete(favorite);
                    favBtn.setImageResource(R.drawable.ic_favorite_border);
                } else {
                    Toast.makeText(getApplicationContext(), "Favorite Added!", Toast.LENGTH_SHORT).show();

                    MovieFavorite newFavorite = new MovieFavorite();
                    newFavorite.setTmdbId(movie_id);
                    newFavorite.setTitle(toolbar.getTitle().toString());
                    newFavorite.setPosterPath(bundle.getString("posterPath"));

                    MyFilm.database.mMovieFavoriteDao().insert(newFavorite);
                    favBtn.setImageResource(R.drawable.ic_favorite);
                }
            }
        });

        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    private void loadMovie() {
        AndroidNetworking.get(Api.MOVIE(movie_id))
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            DownloadImage.picasso(Api.POSTER_PATH + response.getString("backdrop_path"), coverImage);
                            DownloadImage.picasso(Api.POSTER_PATH + response.getString("poster_path"), posterImage);
                            dateText.setText(response.getString("release_date"));
                            durationText.setText(response.getString("runtime"));
                            ratingText.setText(response.getString("vote_average"));
                            sinopsisText.setText(response.getString("overview"));

                            JSONArray jsonArray = response.getJSONObject("videos").getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);

                                Video video = new Video();
                                video.setId(jsonObject.optString("id"));
                                video.setKey(jsonObject.optString("key"));
                                video.setName(jsonObject.optString("name"));
                                videos.add(video);
                            }

                            VideoAdapter videoAdapter = new VideoAdapter(videos);
                            LinearLayoutManager layoutManager = new LinearLayoutManager(MovieDetailActivity.this);
                            videoList.setLayoutManager(layoutManager);
                            videoList.setAdapter(videoAdapter);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_movie_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_comment:
                Intent n = new Intent(getApplicationContext(), CommentActivity.class);
                n.putExtra("movie_id", movie_id);
                startActivity(n);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void mClick(Video video) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + video.getKey())));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
