package com.khoiron.myfilm.utils;

import android.widget.ImageView;

import com.khoiron.myfilm.R;
import com.squareup.picasso.Picasso;

public class DownloadImage {
    public static void picasso(String url, ImageView imageView) {
        Picasso.get().load(url)
                .placeholder(R.drawable.ic_camera)
                .error(R.drawable.ic_error)
                .fit().centerCrop().into(imageView);
    }
}
