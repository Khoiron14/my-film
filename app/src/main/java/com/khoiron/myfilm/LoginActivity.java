package com.khoiron.myfilm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {
    @BindView(R.id.progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.edt_email) EditText mInputEmail;
    @BindView(R.id.edt_password) EditText mInputPassword;
    @BindView(R.id.btn_login) Button mBtnLogin;
    @BindView(R.id.btn_register) Button mBtnRegister;

    private FirebaseAuth mFirebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        mFirebaseAuth = FirebaseAuth.getInstance();

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mInputEmail.getText().toString().equals("") || mInputPassword.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Semua inputan harus diisi!", Toast.LENGTH_LONG).show();
                } else {
                    signin();
                }
            }
        });

        mBtnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });

        getSupportActionBar().setTitle("Login");
    }

    private void signin() {
        mProgressBar.setVisibility(View.VISIBLE);
        mBtnLogin.setEnabled(false);
        mBtnRegister.setEnabled(false);

        mFirebaseAuth.signInWithEmailAndPassword(
                mInputEmail.getText().toString(),
                mInputPassword.getText().toString())
                .addOnSuccessListener(new OnSuccessListener<AuthResult>() {
                    @Override
                    public void onSuccess(AuthResult authResult) {
                        mProgressBar.setVisibility(View.GONE);
                        finish();
                        Toast.makeText(getApplicationContext(), "Login Berhasil!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgressBar.setVisibility(View.GONE);
                        mBtnLogin.setEnabled(true);
                        mBtnRegister.setEnabled(true);
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
