package com.khoiron.myfilm;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.khoiron.myfilm.adapter.CommentAdapter;
import com.khoiron.myfilm.data.model.Comment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends AppCompatActivity {

    @BindView(R.id.progress_bar) ProgressBar mProgressBar;
    @BindView(R.id.list_comment) RecyclerView mListComments;
    @BindView(R.id.edt_comment) EditText mTextComment;
    @BindView(R.id.fab_send) FloatingActionButton mFabSend;

    private FirebaseFirestore mFirebaseFirestore;
    private FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
    ArrayList<Comment> mComments = new ArrayList<>();
    String movieId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        movieId = bundle.getString("movie_id");

        mFirebaseFirestore = FirebaseFirestore.getInstance();

        loadComments();

        mFabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentUser != null) {
                    DocumentReference user = mFirebaseFirestore.collection("users").document(currentUser.getUid());
                    newComment(user);
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
            }
        });

        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        getSupportActionBar().setTitle("Comment");
    }

    private void newComment(DocumentReference user) {
        user.get()
                .addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
                    @Override
                    public void onSuccess(DocumentSnapshot documentSnapshot) {
                        if (!mTextComment.getText().toString().equals("")) {
                            mFabSend.setEnabled(false);
                            Comment newComment = new Comment();
                            newComment.setUid(currentUser.getUid());
                            newComment.setTmdbid(movieId);
                            newComment.setUsername(documentSnapshot.getString("username"));
                            newComment.setImguser(documentSnapshot.getString("imageurl"));
                            newComment.setMessage(mTextComment.getText().toString());

                            Date date = Calendar.getInstance().getTime();
                            DateFormat dateFormat = new SimpleDateFormat("dd MMMM yyy, HH:mm");
                            String currentDate = dateFormat.format(date);

                            newComment.setTime(currentDate);
                            newComment.setTimestamp(date);

                            mFirebaseFirestore.collection("comments/").document()
                                    .set(newComment)
                                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                                        @Override
                                        public void onSuccess(Void aVoid) {
                                            Toast.makeText(getApplicationContext(), "Sukses comment", Toast.LENGTH_LONG).show();
                                            loadComments();
                                            mFabSend.setEnabled(true);
                                            mTextComment.setText("");
                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                                            mFabSend.setEnabled(true);
                                            mTextComment.setText("");
                                        }
                                    });
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        mFabSend.setEnabled(true);
                        mTextComment.setText("");
                    }
                });
    }

    private void loadComments() {
        mProgressBar.setVisibility(View.VISIBLE);
        mComments.clear();

        mFirebaseFirestore.collection("comments")
                .whereEqualTo("tmdbid", movieId)
                .orderBy("timestamp", Query.Direction.DESCENDING)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        for (DocumentSnapshot comments : queryDocumentSnapshots) {
                            if (comments != null) {
                                Comment comment = new Comment();
                                comment.setUid(comments.getString("uid"));
                                comment.setTmdbid(comments.getString("tmdbid"));
                                comment.setUsername(comments.getString("username"));
                                comment.setImguser(comments.getString("imguser"));
                                comment.setMessage(comments.getString("message"));
                                comment.setTime(comments.getString("time"));
                                comment.setTimestamp(comments.getDate("timestamp"));
                                mComments.add(comment);
                            }
                        }

                        CommentAdapter commentAdapter = new CommentAdapter(mComments);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(CommentActivity.this);
                        mListComments.setLayoutManager(layoutManager);
                        mListComments.setAdapter(commentAdapter);
                        mProgressBar.setVisibility(View.GONE);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_LONG).show();
                        mProgressBar.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
