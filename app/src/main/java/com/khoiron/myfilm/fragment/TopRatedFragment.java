package com.khoiron.myfilm.fragment;


import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.khoiron.myfilm.R;
import com.khoiron.myfilm.adapter.MovieAdapter;
import com.khoiron.myfilm.adapter.listener.EndlessOnScrollListener;
import com.khoiron.myfilm.data.Api;
import com.khoiron.myfilm.data.model.Movie;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TopRatedFragment extends Fragment {
    private ProgressBar mProgressBar;
    private RecyclerView mRecyclerView;
    private MovieAdapter mAdapter;
    private Parcelable recyclerViewState;
    public RecyclerView.LayoutManager gridLayoutManager;

    ArrayList<Movie> movies = new ArrayList<>();

    public TopRatedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        mRecyclerView = view.findViewById(R.id.list_movie);
        mProgressBar = view.findViewById(R.id.progress_bar);
        gridLayoutManager = new GridLayoutManager(getContext(), 2);

        mRecyclerView.addOnScrollListener(scrollData());

        // get first page movie top rated
        fan(Api.MOVIE_TOPRATED("1"));
        return view;
    }

    private EndlessOnScrollListener scrollData() {
        return new EndlessOnScrollListener() {
            @Override
            public void onLoadMore() {
                int itemLoad = 20;
                int nextPage = (movies.size() / itemLoad) + 1;

                fan(Api.MOVIE_TOPRATED(String.valueOf(nextPage)));
            }
        };
    }

    private void fan(String url) {
        mProgressBar.setVisibility(View.VISIBLE);

        AndroidNetworking.get(url)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            JSONArray jsonArray = response.getJSONArray("results");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                Movie movie = new Movie();
                                movie.setId(jsonObject.optString("id"));
                                movie.setPoster(jsonObject.optString("poster_path"));
                                movie.setTitle(jsonObject.optString("title"));
                                movies.add(movie);
                            }

                            MovieAdapter movieAdapter = new MovieAdapter(movies);

                            mAdapter = new MovieAdapter(movies);

                            mRecyclerView.setLayoutManager(gridLayoutManager);
                            recyclerViewState = mRecyclerView.getLayoutManager().onSaveInstanceState();
                            mRecyclerView.setAdapter(mAdapter);

                            mRecyclerView.getLayoutManager().onRestoreInstanceState(recyclerViewState);
                            mProgressBar.setVisibility(View.GONE);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }

}
