package com.khoiron.myfilm.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.khoiron.myfilm.R;
import com.khoiron.myfilm.adapter.FavoriteMovieAdapter;
import com.khoiron.myfilm.data.MyFilm;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteFragment extends Fragment {

    RecyclerView mRecyclerView;
    ProgressBar mProgressBar;

    public FavoriteFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        mRecyclerView = view.findViewById(R.id.list_movie);
        mProgressBar = view.findViewById(R.id.progress_bar);

        loadMovieFavorites();

        return view;
    }

    private void loadMovieFavorites() {
        mProgressBar.setVisibility(View.VISIBLE);
        mRecyclerView.setAdapter(null);
        FavoriteMovieAdapter favoriteMovieAdapter = new FavoriteMovieAdapter(MyFilm.database.mMovieFavoriteDao().getAll());

        mRecyclerView.setLayoutManager(new GridLayoutManager(getContext(),2));
        mRecyclerView.setAdapter(favoriteMovieAdapter);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadMovieFavorites();
    }
}
